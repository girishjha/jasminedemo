# Karma/Jasmine demo.

###Unit and End-to-End Testing in AngularJS

#### What is it?

*Taken from Sandeep Panda's book - AngularJS: Novice to Ninja*


`Unit testing is a technique that lets developers validate isolated pieces of code. The unit is the smallest testable part of your code. So, while you're coding it's always a good idea to validate and ensure that your code works as expected. For example, when you write a controller it's smart to unit test it first before moving to next thing. This way, if you need to add some more functionality to the controller, you can do so and also validate that you haven't broken any of the previous functionalities in this process.`

`End-to-End testing helps ascertain that a set of components, when integrated together, work as expected. End-to-End tests should be carried out when user stories are being generated. For instance, an End-to-End test can be done to ensure when a user enters correct username, password and hits login button he is taken to the admin panel. End-to-End tests differ from unit testing in that they take two or more pieces of unit-tested code and ensure that they work as expected when integrated together.`


#### Some important points:

- Jasmine is a unit testing framework for JavaScript. A .NET analogy will be NUnit.
- Karma is a test runner.
- Karma runs on Node.JS.


####Setting it up

- Install Node.JS
- Once node installed run the following commands in the console


```
#!shell

  npm install -g karma  --save-dev  
  npm install -g karma-cli  --save-dev 
  npm install -g karma-coverage --save-dev 
  npm install -g phantomjs  --save-dev 
  npm install -g karma-jasmine --save-dev 
  npm install -g karma-phantomjs-launcher --save-dev 
  npm install -g karma-ng-html2js-preprocessor --save-dev
```


Following packages are non-mandatory, but it's recommended to have if you want to debug the JS tests.


```
#!shell

  npm install -g karma-firefox-launcher --save-dev 
  npm install -g karma-chrome-launcher --save-dev
  npm install –g karma-ie-launcher --save-dev
```

`


- once karma is installed run the navigate to your project directory and run the following command to create a new karma config file. This file contains all the configs for karma to start and execute your tests.

		karma init karma.conf.js

- now to run the the test use following command. Karma will read the configuration file and run the tests in your 

		karma start karma.conf.js 

## Useful links 

Jasmine documentations
http://jasmine.github.io/2.0/introduction.html

For information on mocking : 
http://www.sitepoint.com/mocking-dependencies-angularjs-tests/

Blog on making a http service and write tests for it
http://nathanleclaire.com/blog/2014/04/12/unit-testing-services-in-angularjs-for-fun-and-for-profit/


The ng-newsLetter on angular JS unit testing with jasmine
http://www.ng-newsletter.com/advent2013/#!/day/19

More read
https://quickleft.com/blog/angularjs-unit-testing-for-real-though/