"use strict";

SortingModule.controller("MergeSortController", function($scope, MergeSortService, $array) {

	$scope.MergeSort = function(unsortedList) {
		if (unsortedList.length <= 1)
			return unsortedList;
		var leftHalf = $scope.MergeSort($array.leftHalf(unsortedList));
		var rightHalf = $scope.MergeSort($array.rightHalf(unsortedList));
		return MergeSortService.Merge(leftHalf, rightHalf);
	};



});