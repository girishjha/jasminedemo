"use strict";  

angular.module("MyModule", [])
    .service("myService", function(){
        var service = {};
        service.leftHalf = function(unsortedList){
            if(unsortedList.length < 2)
                return unsortedList;
            return unsortedList.slice(0, unsortedList.length /2);
        };

        service.rightHalf = function(unsortedList){
            if(unsortedList.length < 2)
                return unsortedList;
            return unsortedList.slice(unsortedList.length /2, unsortedList.length);
        };

        return service;
    })

    .controller("MyController", function($scope, myService){

    $scope.MergeSort = function(unsortedList){
        if(unsortedList.length <= 1)
            return unsortedList;
        var leftHalf = $scope.MergeSort( $scope.leftHalf(unsortedList));
        var rightHalf = $scope.MergeSort($scope.rightHalf(unsortedList));
        return $scope.Merge(leftHalf,rightHalf);
    };

    $scope.Merge = function(leftList, rightList){
        var result = [];
        result.pop();
        while(leftList.length > 0 && rightList.length > 0){
            if(leftList[0] < rightList[0])
                result.push(leftList.shift());
            else result.push(rightList.shift());
        }

        if(leftList.length > 0)
            result = result.concat(leftList);
        if(rightList.length > 0)
            result = result.concat(rightList);

        return result;
    };

        $scope.leftHalf = function(unsortedList){
            if(unsortedList.length < 2)
                return unsortedList;
            return unsortedList.slice(0, unsortedList.length /2);
        };

        $scope.rightHalf = function(unsortedList){
            if(unsortedList.length < 2)
                return unsortedList;
            return unsortedList.slice(unsortedList.length /2, unsortedList.length);
        };
});