SortingModule.service("MergeSortService", function() {
	
	this.Merge = function(leftList, rightList){
        var result = [];
        result.pop();
        while(leftList.length > 0 && rightList.length > 0){
            if(leftList[0] < rightList[0])
                result.push(leftList.shift());
            else result.push(rightList.shift());
        }

        if(leftList.length > 0)
            result = result.concat(leftList);
        if(rightList.length > 0)
            result = result.concat(rightList);

        return result;
    };

   

});