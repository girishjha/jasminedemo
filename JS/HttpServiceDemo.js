"use strict";

//$http.get $ $http.post() or $http() returns a promise object which has methods like then(), success() and error()
//the signature of then() is : then(successCallback, errorCallback, notifyCallback)
/* where then() / success() / error()/ again returns a promise so that these methods can be chained like:
 	
 	  $http.get(url)
 		.success(function(data){
			do your stuffs with data
 		})
 		.error(function(errorMsg){
			do your error handling
 		});


*/

//The best way to use the http in our application would be injecting the specific http service to the controller and returning the promise back
//to the controller so that the controller in in control of the data which it requested for or gracefully handles the boo boo situation.


angular.module('weatherModule', [])
	.constant('CityID', {
		Delhi: 'as if i care',
		Mumbai: '1275339',
		Bangalore: '6695236'
	})
	.service('weatherHttpService', function($http, CityID) {

		this.GetWeatherInfoForDelhi = function() {
			return $http.get('http://api.openweathermap.org/data/2.5/weather?id=' + CityID.Delhi)
				.then(function(response) {
					return response.data.cod + 5;
				});
		};
	});