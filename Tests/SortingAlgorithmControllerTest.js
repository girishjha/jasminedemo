"use strict";

describe('merge sort controller test', function() {

	var controller, scope, mergeSortService, arrayService;

	beforeEach(function() {
		module("SortingModule");
		inject(function(MergeSortService, array) {
			arrayService = array;
			mergeSortService = MergeSortService;
		});
		inject(function($controller, $rootScope) {
			scope = $rootScope.$new();
			controller = $controller("MergeSortController", {
				$scope: scope,
				MergeSortService: mergeSortService,
				$array: arrayService
			});

		});
	});

	xit('should sort properly', function() {
		var inputArray = [3, 2, 1, 6, 7, 4];
		var expected = [1, 2, 3, 4, 6, 7];
		var actual = scope.MergeSort(inputArray);
		expect(expected).toEqual(actual);
	});

	it('should sort properly using Mocks', function() {
		var inputArray = [3, 2, 1, 6, 7, 4];
		var expected = [1, 2, 3, 4, 6, 7];
		spyOn(arrayService, 'leftHalf').and.callFake(function(inputArray) {
			if (inputArray.length < 2)
				return inputArray;
			return inputArray.slice(0, inputArray.length / 2);
		});

		var actual = scope.MergeSort(inputArray);
		expect(expected).toEqual(actual);
	});
});