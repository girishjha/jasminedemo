describe('Should show the http service testing as well as mocck the value in the module', function() {
	var cityIDMock, httpMock, service;

	beforeEach(module('weatherModule'));
	beforeEach(module('weatherModule', function($provide) {
		$provide.constant("CityID", {
			Delhi: jasmine.createSpy('Delhi'),
			Mumbai: jasmine.createSpy('Mumbai')
		});


	}));

	beforeEach(inject(function(_weatherHttpService_, CityID, $httpBackend) {
		httpMock = $httpBackend;
		service = _weatherHttpService_;
		cityIDMock = CityID;
	}));



	it('Should call the weatherAPI', function() {
		httpMock
			.whenGET('http://api.openweathermap.org/data/2.5/weather?id=' + cityIDMock.Delhi)
			.respond({
				"cod": 10
			});

		service.GetWeatherInfoForDelhi().then(function(result) {
			expect(15).toEqual(result);
		});

		httpMock.flush();
	});
});