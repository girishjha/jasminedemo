'use strict';
describe("mergeSort algorithm", function() {
	beforeEach(module("MyModule"));

	var controller, scope;


	beforeEach(inject(function($controller) {
		scope = {};
		controller = $controller("MyController", {
			$scope: scope
		});

	}));

	it("should sort the array by calling mergeSort", function() {
		var inputArray = [4, 3, 6, 2, 7, 8];
		var actualResult = scope.MergeSort(inputArray);
		expect(actualResult[0]).toEqual(2);
		expect(actualResult[1]).toEqual(3);
	});

	it("should merge two  given array", function() {
		var arr1 = [2, 6];
		var arr2 = [4, 7];

		var actualResult = scope.Merge(arr1, arr2);

		expect(actualResult[0]).toEqual(2);
	});


});